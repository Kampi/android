package com.kampis_elektroecke.customviewexample;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity
{
    private CustomView _mCustomView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        _mCustomView = findViewById(R.id.CustomView);
    }
}